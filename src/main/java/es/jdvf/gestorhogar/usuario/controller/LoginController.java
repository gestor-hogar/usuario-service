package es.jdvf.gestorhogar.usuario.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.jdvf.gestorhogar.usuario.dto.Usuario;
import es.jdvf.gestorhogar.usuario.exception.FamiliaNotFoundException;
import es.jdvf.gestorhogar.usuario.service.UsuarioService;
import io.swagger.annotations.ApiOperation;

/**
 * Controller gestión de familia
 *
 */
@RestController
@RequestMapping("/api/login")
public class LoginController {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private UsuarioService service;
	
	/**
	 * Crea un usuario
	 * 
	 * @param usuario
	 * @return
	 */
	@ApiOperation(value = "Crea/Registra un usuario.", response = Usuario.class)
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Usuario> crear(@RequestBody @Valid Usuario usuario) {
		LOGGER.info("create - begin - usuario: {}", usuario);
		Usuario created = service.crear(usuario);
		LOGGER.info("create - result: {}", created);
		return ResponseEntity.ok(created);
	}
	
	/**
	 * Recupera contraseña de usuario
	 * 
	 * @param usuario
	 * @return
	 */
	@ApiOperation(value = "Crea/Registra un usuario.", response = Boolean.class)
	@RequestMapping(method = RequestMethod.GET, path="/{email}/recuperar")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Boolean> recuperar(@PathVariable("email") String email) {
		LOGGER.info("create - begin - email: {}", email);
		service.recuperar(email);
		LOGGER.info("create - end: ");
		return ResponseEntity.ok(Boolean.TRUE);
	}
	
	/**
	 * login un usuario
	 * 
	 * @param usuario
	 * @return
	 */
	@ApiOperation(value = "Validación de usuario.", response = Usuario.class)
	@RequestMapping(method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<Usuario> login(@RequestParam(value = "email") String email, @RequestParam(value = "password") String password) {
		LOGGER.info("login - begin");
		Usuario usuario = service.login(email, password);
		LOGGER.info("login - result: {}", usuario);
		return ResponseEntity.ok(usuario);
	}
	
	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleTodoNotFound(FamiliaNotFoundException ex) {
		LOGGER.error("Handling error with message: {}", ex.getMessage());
	}
}
