package es.jdvf.gestorhogar.usuario.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.jdvf.gestorhogar.usuario.dto.Familia;
import es.jdvf.gestorhogar.usuario.dto.Usuario;
import es.jdvf.gestorhogar.usuario.exception.UsuarioNotFoundException;
import es.jdvf.gestorhogar.usuario.service.UsuarioService;
import io.swagger.annotations.ApiOperation;

/**
 * Controller gestión de usuario
 *
 */
@RestController
@RequestMapping("/api/usuario")
public class UsuarioController {
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	private UsuarioService service;

	/**
	 * Envia invitación a un  usuario
	 * 
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "Envia invitación a un  usuario.", response = Boolean.class)
	@RequestMapping(value = "/{email}/invitar", method = RequestMethod.GET)
	public ResponseEntity<Boolean> invitar(@PathVariable("email") String email) {
		LOGGER.info("invitar - begin - email: {}", email);
		service.invitar(email);
		LOGGER.info("invitar - end");
		return ResponseEntity.ok(Boolean.TRUE);
	}
	
	/**
	 * Modificar datos del usuario
	 * 
	 * @param usuario
	 * @return
	 */
	@ApiOperation(value = "Modificar datos del usuario.", response = Usuario.class)
	@RequestMapping(value = "/", method = RequestMethod.PUT)
	public ResponseEntity<Usuario> modificar(@RequestBody @Valid Usuario usuario) {
		LOGGER.info("modificar - begin - usuario: {}", usuario);
		Usuario result = service.modificar(usuario);
		LOGGER.info("modificar - result: {}", result);
		return ResponseEntity.ok(result);
	}
	
	/**
	 * Modificar datos de la familia
	 * 
	 * @param familia
	 * @return
	 */
	@ApiOperation(value = "Modificar datos de la familia.", response = Familia.class)
	@RequestMapping(value = "/modificarFamilia", method = RequestMethod.PUT)
	public ResponseEntity<Familia> modificarFamilia(@RequestBody @Valid Familia familia) {
		LOGGER.info("modificarFamilia - begin - familia: {}", familia);
		Familia result = service.modificar(familia);
		LOGGER.info("modificarFamilia - result: {}", result);
		return ResponseEntity.ok(result);
	}
	
	/**
	 * Cambiar administrador de la familia.
	 * 
	 * @param familia
	 * @return
	 */
	@ApiOperation(value = "Cambiar administrador de la familia.", response = Boolean.class)
	@RequestMapping(value = "/cambiarAdmin/{email}", method = RequestMethod.PUT)
	public ResponseEntity<Boolean> cambiarAdmin(@PathVariable("email") String email) {
		LOGGER.info("cambiarAdmin - begin - email: {}", email);
		service.cambiarAdmin(email);
		LOGGER.info("cambiarAdmin - end");
		return ResponseEntity.ok(Boolean.TRUE);
	}
	
	/**
	 * Eliminar usuario.
	 * 
	 * @ email
	 * @return
	 */
	@ApiOperation(value = " Eliminar usuario.", response = Boolean.class)
	@RequestMapping(value = "/{email}", method = RequestMethod.DELETE)
	public ResponseEntity<Boolean> eliminar(@PathVariable("email") String email) {
		LOGGER.info("eliminar - begin - email: {}", email);
		service.eliminar(email);
		LOGGER.info("eliminar - end");
		return ResponseEntity.ok(Boolean.TRUE);
	}

	/**
	 * Devuelve lista de usuarios
	 * 
	 * @return
	 */
	@ApiOperation(value = "Devuelve lista de usuarios.", response = Usuario.class, responseContainer = "List")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> findAll() {
		LOGGER.info("findAll - begin");
		List<Usuario> usuarios = service.findAll();
		LOGGER.info("findAll - result: {}", usuarios);
		return ResponseEntity.ok(usuarios);
	}

	/**
	 * Devuelve detalle de un usuario
	 * 
	 * @param id
	 *            identificador de usuario
	 * @return
	 */
	@ApiOperation(value = "Devuelve detalle de un usuario.", response = Usuario.class)
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> findById(@PathVariable("id") String id) {
		LOGGER.info("findById - begin - id: {}", id);
		Usuario usuario = service.findById(id);
		LOGGER.info("findById - result: {}", usuario);
		return ResponseEntity.ok(usuario);
	}

	/**
	 * Actualiza datos de un usuario
	 * 
	 * @param usuario
	 *            datos del usuario
	 * @return
	 */
	@ApiOperation(value = "Actualiza datos de un usuario.", response = Usuario.class)
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public ResponseEntity<Usuario> update(@RequestBody @Valid Usuario usuario) {
		LOGGER.info("update - begin - usuario: {}", usuario);
		Usuario updated = service.update(usuario);
		LOGGER.info("update - result: {}", updated);
		return ResponseEntity.ok(updated);
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public void handleTodoNotFound(UsuarioNotFoundException ex) {
		LOGGER.error("Handling error with message: {}", ex.getMessage());
	}
}
