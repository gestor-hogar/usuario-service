package es.jdvf.gestorhogar.usuario.util;

import static java.util.stream.Collectors.toList;

import java.util.List;

import es.jdvf.gestorhogar.usuario.dto.Familia;
import es.jdvf.gestorhogar.usuario.dto.Usuario;
import es.jdvf.gestorhogar.usuario.entity.FamiliaEntity;
import es.jdvf.gestorhogar.usuario.entity.UsuarioEntity;

public class UsuarioUtil {

	/**
	 * Devuelve objeto DTO usuario a partir de la entidad
	 * @param entity
	 * @return
	 */
	public static Usuario convertToDTO(UsuarioEntity entity) {
		return Usuario.builder().id(entity.getId())
				.nombre(entity.getNombre())
				.apellido1(entity.getApellido1())
				.apellido2(entity.getApellido2())
				.email(entity.getEmail())
				.telefono(entity.getTelefono())
				.build();
	}
	
	/**
	 * Devuelve objeto entidad usuario a partir del DTO
	 * @param dto
	 * @return
	 */
	public static UsuarioEntity convertToEntity(Usuario dto) {
		return UsuarioEntity.builder().id(dto.getId())
				.nombre(dto.getNombre())
				.apellido1(dto.getApellido1())
				.apellido2(dto.getApellido2())
				.email(dto.getEmail())
				.telefono(dto.getTelefono())
				.build();
	}

	/**
	 * Devuelve lista de objetos DTO usuario a partir de lista de entidades
	 * @param entities
	 * @return
	 */
	public static List<Usuario> convertToDTOs(List<UsuarioEntity> entities) {
        return entities.stream()
                .map(UsuarioUtil::convertToDTO)
                .collect(toList());
	}
	
	/**
	 * Devuelve objeto DTO familia a partir de la entidad
	 * @param entity
	 * @return
	 */
	public static Familia convertToDTO(FamiliaEntity entity) {
		return Familia.builder().id(entity.getId())
				.nombre(entity.getNombre())
				.build();
	}
	
	/**
	 * Devuelve objeto entidad familia a partir del DTO
	 * @param dto
	 * @return
	 */
	public static FamiliaEntity convertToEntity(Familia dto) {
		return FamiliaEntity.builder().id(dto.getId())
				.nombre(dto.getNombre())
				.build();
	}

	/**
	 * Devuelve lista de objetos DTO familia a partir de lista de entidades
	 * @param entities
	 * @return
	 */
	public static List<Familia> convertToDTOsFam(List<FamiliaEntity> entities) {
        return entities.stream()
                .map(UsuarioUtil::convertToDTO)
                .collect(toList());
	}
}
