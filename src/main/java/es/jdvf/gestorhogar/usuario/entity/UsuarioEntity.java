package es.jdvf.gestorhogar.usuario.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(collection = "usuario")
public class UsuarioEntity {
	// Identificador de usuario
	private String id;
	// Nombre
	private String nombre;
	// Primer apellido
	private String apellido1;
	// Segundo apellido
	private String apellido2;
	// email
	private String email;
	// Teléfono
	private String telefono;
	// Rol
	private String rol;
	// Lista de familias a la que pertenece
	private List<String> familias;
}
