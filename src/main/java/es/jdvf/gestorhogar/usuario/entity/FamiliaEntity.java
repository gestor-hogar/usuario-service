package es.jdvf.gestorhogar.usuario.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Document(collection = "familia")
public class FamiliaEntity {
	// Identificador de familia
	private String id;
	// Nombre
	private String nombre;
	// Lista de usuarios que contiene
	private List<String> usuarios;
}