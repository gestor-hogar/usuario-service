package es.jdvf.gestorhogar.usuario.exception;

/**
 * This exception is thrown when the requested familia entity is not found.
 */
public class FamiliaNotFoundException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FamiliaNotFoundException(String id) {
        super(String.format("No familia entity found with id: <%s>", id));
    }
}
