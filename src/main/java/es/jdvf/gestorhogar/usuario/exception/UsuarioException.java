package es.jdvf.gestorhogar.usuario.exception;

/**
 * The Class ClienteException.
 */
public class UsuarioException extends Exception {

    /** The error code. */
    private final Integer errorCode;

    /** The lista error. */
    private final Object errorList;

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new cliente exception.
     *
     * @param errorCode the error code
     * @param errorList the error list
     */
    public UsuarioException(Integer errorCode, Object errorList) {
        super();
        this.errorCode = errorCode;
        this.errorList = errorList;
    }

    /**
     * Instantiates a new cliente exception.
     *
     * @param errorCode the error code
     * @param errorList the error list
     * @param message the message
     * @param cause the cause
     */
    public UsuarioException(Integer errorCode, Object errorList, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorList = errorList;
    }

    /**
     * Instantiates a new cliente exception.
     *
     * @param errorCode the error code
     * @param message the message
     * @param cause the cause
     */
    public UsuarioException(Integer errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
        this.errorList = null;
    }

    /**
     * Instantiates a new cliente exception.
     *
     * @param errorCode the error code
     * @param errorList the error list
     * @param message the message
     */
    public UsuarioException(Integer errorCode, Object errorList, String message) {
        super(message);
        this.errorCode = errorCode;
        this.errorList = errorList;
    }

    /**
     * Instantiates a new cliente exception.
     *
     * @param errorCode the error code
     * @param message the message
     */
    public UsuarioException(Integer errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        this.errorList = null;
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public Integer getErrorCode() {
        return errorCode;
    }

    /**
     * Gets the error list.
     *
     * @return the error list
     */
    public Object getErrorList() {
        return errorList;
    }

}
