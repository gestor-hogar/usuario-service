package es.jdvf.gestorhogar.usuario.exception;

/**
 * This exception is thrown when the requested usuario entity is not found.
 */
public class UsuarioNotFoundException extends RuntimeException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNotFoundException(String id) {
        super(String.format("No usuario entity found with id: <%s>", id));
    }
}
