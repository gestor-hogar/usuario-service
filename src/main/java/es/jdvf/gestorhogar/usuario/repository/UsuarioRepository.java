package es.jdvf.gestorhogar.usuario.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.Repository;

import es.jdvf.gestorhogar.usuario.entity.UsuarioEntity;

public interface UsuarioRepository extends Repository<UsuarioEntity, String> {

	void delete(UsuarioEntity deleted);

	List<UsuarioEntity> findAll();

	Optional<UsuarioEntity> findOne(String id);

	UsuarioEntity save(UsuarioEntity saved);
}
