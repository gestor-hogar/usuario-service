package es.jdvf.gestorhogar.usuario.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.Repository;

import es.jdvf.gestorhogar.usuario.entity.FamiliaEntity;

public interface FamiliaRepository extends Repository<FamiliaEntity, String> {

	void delete(FamiliaEntity deleted);

	List<FamiliaEntity> findAll();

	Optional<FamiliaEntity> findOne(String id);

	FamiliaEntity save(FamiliaEntity saved);
}
