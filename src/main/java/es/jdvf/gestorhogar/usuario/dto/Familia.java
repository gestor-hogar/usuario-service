package es.jdvf.gestorhogar.usuario.dto;

import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Familia {
	// Identificador de familia
	@Id
	private String id;
	// Nombre de familia
	@NotEmpty
	private String nombre;
	// Lista de usuarios integrantes
	private List<Usuario> usuarios;
}
