package es.jdvf.gestorhogar.usuario.dto;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Usuario {
	// Identificador de usuario
	@Id
	private String id;
	// Nombre
	@NotEmpty
	private String nombre;
	// Primer apellido
	@NotEmpty
	private String apellido1;
	// Segundo apellido
	private String apellido2;
	// email
	@NotEmpty
	private String email;
	// Teléfono
	private String telefono;
	// Rol
	private String rol;
}
