package es.jdvf.gestorhogar.usuario.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.jdvf.gestorhogar.usuario.dto.Familia;
import es.jdvf.gestorhogar.usuario.dto.Usuario;
import es.jdvf.gestorhogar.usuario.entity.UsuarioEntity;
import es.jdvf.gestorhogar.usuario.exception.UsuarioException;
import es.jdvf.gestorhogar.usuario.exception.UsuarioNotFoundException;
import es.jdvf.gestorhogar.usuario.repository.UsuarioRepository;
import es.jdvf.gestorhogar.usuario.util.UsuarioUtil;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioServiceImpl.class);

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public Usuario create(Usuario usuario) throws UsuarioException {
		LOGGER.debug("create - begin - input: {}", usuario);
		UsuarioEntity persisted = UsuarioUtil.convertToEntity(usuario);
		// Comprobar si ya existe ese usuario
		if (findUsuarioById(usuario.getEmail()) != null) {
			// Usuario ya existe, lanzar excepción
			throw new UsuarioException(500, "Error: usuario ya existe en la aplciación");
		}
		usuarioRepository.save(persisted);
		Usuario result = UsuarioUtil.convertToDTO(persisted);
		LOGGER.debug("create - result: {}", result);
		return result;
	}

	@Override
	public Usuario delete(String id) {
		LOGGER.debug("delete - begin - id: {}", id);
		UsuarioEntity deleted = findUsuarioById(id);
		usuarioRepository.delete(deleted);
		Usuario result = UsuarioUtil.convertToDTO(deleted);
		LOGGER.debug("delete - result: {}");
		return result;
	}

	@Override
	public List<Usuario> findAll() {
		LOGGER.debug("findAll - begin");
		List<UsuarioEntity> entities = usuarioRepository.findAll();
		List<Usuario> result = UsuarioUtil.convertToDTOs(entities);
		LOGGER.debug("findAll - end - result: {}", result != null ? result.toString() : result);
		;
		return result;
	}

	@Override
	public Usuario findById(String id) {
		LOGGER.debug("findById - begin - id :{}", id);
		UsuarioEntity entity = findUsuarioById(id);
		Usuario result = UsuarioUtil.convertToDTO(entity);
		LOGGER.debug("findById - result: {}", result);
		return result;
	}

	@Override
	public Usuario update(Usuario usuario) {
		LOGGER.debug("update - begin - input: {}", usuario);
		UsuarioEntity updated = findUsuarioById(usuario.getId());
		// Actualizar datos de usuario
		updated.setNombre(usuario.getNombre());
		updated.setApellido1(usuario.getApellido1());
		updated.setApellido2(usuario.getApellido2());
		updated.setEmail(usuario.getEmail());
		updated.setTelefono(usuario.getTelefono());
		updated = usuarioRepository.save(updated);
		Usuario result = UsuarioUtil.convertToDTO(updated);
		LOGGER.debug("update - RESULT: {}", result);
		return result	;
	}

	private UsuarioEntity findUsuarioById(String id) {
		Optional<UsuarioEntity> result = usuarioRepository.findOne(id);
		return result.orElseThrow(() -> new UsuarioNotFoundException(id));
	}

	@Override
	public Usuario crear(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void recuperar(String email) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario login(String email, String password) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(String email) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void invitar(String email) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Usuario modificar(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Familia modificar(Familia familia) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void cambiarAdmin(String email) {
		// TODO Auto-generated method stub
		
	}

}
