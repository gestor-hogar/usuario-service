package es.jdvf.gestorhogar.usuario.service;

import java.util.List;

import es.jdvf.gestorhogar.usuario.dto.Familia;
import es.jdvf.gestorhogar.usuario.dto.Usuario;
import es.jdvf.gestorhogar.usuario.exception.UsuarioException;

public interface UsuarioService {

	/**
	 * Crea un usuario en BBDD
	 * @param usuario
	 * @return
	 * @throws UsuarioException 
	 */
    Usuario create(Usuario usuario) throws UsuarioException;
    
    /**
     * Elimina una usuario de BBDD
     * @param id
     * @return
     */
    Usuario delete(String id);
 
    /**
     * Obtiene listado de todos los usuarios de BBDD
     * @return
     */
    List<Usuario> findAll();
 
    /**
     * Obtiene usuario a partir de su identificador
     * @param id
     * @return
     */
    Usuario findById(String id);
 
    /**
     * Actualiza un usuario en BBDD
     * @param usuario
     * @return
     */
    Usuario update(Usuario usuario);

	Usuario crear(Usuario usuario);

	void recuperar(String email);

	Usuario login(String email, String password);

	void eliminar(String email);

	void invitar(String email);

	Usuario modificar(Usuario usuario);
	
	Familia modificar(Familia familia);

	void cambiarAdmin(String email);
}
